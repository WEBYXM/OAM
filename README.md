GO-OAM是基于go语言开发的web版运维资源管理系统,算是简易版的CMDB,将各种难记的**账号、密码、主机、文档、应用**等资源管理起来,以项目方式整合.

## 系统截图

![项目](screenshot/proj.png)

![主机](screenshot/host.png)

![账号](screenshot/account.png)

![账号类型](screenshot/accounttype.png)

## 技术选型:

* 后端：golang, beego
* 前端：jquery, easyui
* 数据库:mysql 或 sqlite

## 安装部署

1. 可选择下载编译好的安装包或自己在编译打包(需要go语言开发环境)

### 打包命令

linux环境:

bee pack -exp=screenshot:logs:.vscode:.git:README.md:tests -a=oam -be GOOS=linux -be GOARCH=amd64

编译后文件在源码要目录下:oam.tar.gz

windows环境:

bee pack -f=zip -exp=screenshot:logs:.vscode:.git:README.md:tests -a=oam -be GOOS=windows -be GOARCH=amd64

编译后文件在源码要目录下:oam.zip

2. 解压安装包,创建数据库
   
数据库文件在data目录下,oam.db为sqlite文件, oam_mysql.sql为mysql脚本

3. 修改配置

配置文件conf/app.conf主要配置项说明:
```
httpport = 8848  # 端口号
runmode = dev    # 当前使用环境,对应下面的[dev/prod]章节不同
...
dbtype=mysql     # 使用的数据库类型:mysql 或sqllite
dbname = "oam"   # 数据库名称(sqlite是文件名)
...

#下面是不同环境的配置变量,主要是数据库信息,其他可以不动
[dev]
debug = "true"
dbuser = "user"
dbpasswd = "123"
dbaddr = "10.18.203.94"
store_path=

[prod]
debug = "false"
dbuser = "user"
dbpasswd = "123"
dbaddr = "10.18.203.32"
store_path=

```

4. 启动

Linux,需要给oam文件执行权限,./oam执行
Window,双击oam.exe文件

启动后浏览器打开http://localhost:8848, 默认登录用户root 密码 2022@00



