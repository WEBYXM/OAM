package controllers

import (
	"OAM/conf"
	"OAM/models"
	"OAM/util"
	"errors"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/beego/beego/v2/core/logs"
	beego "github.com/beego/beego/v2/server/web"
	jsoniter "github.com/json-iterator/go"
	"golang.org/x/exp/slices"
)

type BaseController struct {
	beego.Controller
}

//用jsoniter替换原生json
func (c *BaseController) ServeJSON(encoding ...bool) error {
	var (
		hasIndent = beego.BConfig.RunMode != beego.PROD
		//hasEncoding = len(encoding) > 0 && encoding[0]
	)
	data := c.Data["json"]
	output := c.Ctx.Output
	output.Header("Content-Type", "application/json; charset=utf-8")
	var content []byte
	var err error
	if hasIndent {
		content, err = jsoniter.MarshalIndent(data, "", "  ")
	} else {
		content, err = jsoniter.Marshal(data)
	}
	if err != nil {
		http.Error(output.Context.ResponseWriter, err.Error(), http.StatusInternalServerError)
		return err
	}
	//if encoding {
	//	content = []byte(stringsToJSON(string(content)))
	//}
	return output.Body(content)
}

func (c *BaseController) BindJSON(obj interface{}) error {
	return jsoniter.Unmarshal(c.Ctx.Input.RequestBody, obj)
}

//输出CallResult JSON
func (c *BaseController) JsonResult(r models.CallResult) {
	c.Data["json"] = r
	c.ServeJSON()
	c.StopRun()
}

func (c *BaseController) JsonStatus(status int, message string) {
	r := models.CallResult{Status: status, Message: message}
	c.JsonResult(r)
}

//输出成功返回JSON,数据以CallResult封装
func (c *BaseController) JsonOk(data interface{}) {
	var r models.CallResult
	r.Ok(data)
	c.JsonResult(r)
}

//输出参数错误JSON,数据以CallResult封装
func (c *BaseController) JsonParamError(message string) {
	var r models.CallResult
	r.ParamError(message)
	c.JsonResult(r)
}

func (c *BaseController) JsonFailed(message string) {
	var r models.CallResult
	r.Failed(message)
	c.JsonResult(r)
}

func (c *BaseController) JsonError() {
	var r models.CallResult
	r.Error()
	c.JsonResult(r)
}

func (c *BaseController) JsonForbidden() {
	var r models.CallResult
	r.Forbidden()
	c.JsonResult(r)
}

func (c *BaseController) toErrorPage(errmsg string) {
	c.Data["content"] = errmsg
	c.TplName = "error.html"
}

// 是否POST提交
func (c *BaseController) justPost() {
	if !c.Ctx.Input.IsPost() {
		c.Abort("405")
	}
}

var AllowedImgExts = []string{".jpg", ".jpeg", ".png", ".gif", ".bmp"}
var AllowedDocExts = []string{".pdf", ".doc", ".docx", ".md"}

//图片文件最大限制
const img_max_size int64 = 1024 * 1024 * 3

//文档文件最大限制
const doc_max_size int64 = 1024 * 1024 * 25

//文件上传处理
func (c *BaseController) uploadFile(fileFormName string, fileType int) (string, error) {
	f, h, err := c.GetFile(fileFormName)
	if err != nil {
		logs.Error("上传图片异常", err)
		return "", err
	}

	defer f.Close()

	ext := path.Ext(h.Filename)
	if fileType == 0 {
		if !slices.Contains(AllowedImgExts, strings.ToLower(ext)) {
			return "", errors.New("不支持的图片类型")
		}
		if h.Size > img_max_size {
			return "", errors.New("超过图片限制大小：3M")
		}
	}
	if fileType == 1 {
		if !slices.Contains(AllowedDocExts, strings.ToLower(ext)) {
			return "", errors.New("不支持的文档类型")
		}
		if h.Size > doc_max_size {
			return "", errors.New("超过文件限制大小：25M")
		}
	}
	savePath := getSavePath(fileType)
	newFilePath := filepath.Join(savePath, h.Filename)
	if util.FileIsExists(newFilePath) {
		newFileName := generateAttachName(ext, fileType)
		newFilePath = filepath.Join(savePath, newFileName)
	}
	err = c.SaveToFile(fileFormName, filepath.Join(newFilePath))
	if err != nil {
		return "", err
	} else {
		relpath, err := filepath.Rel(conf.GlobalCfg.DATA_STORE_PATH, newFilePath)
		if err == nil && runtime.GOOS == "windows" {
			relpath = filepath.ToSlash(relpath)
		}
		return relpath, err
	}
}

func getSavePath(fileType int) string {
	dir := strconv.Itoa(int(time.Now().Month()))
	if fileType == 0 {
		dir = "/images/" + dir
	} else {
		dir = "/doc/" + dir
	}
	absDir := filepath.Join(conf.GlobalCfg.DATA_STORE_PATH, dir)
	if !util.FileIsExists(absDir) {
		err := os.MkdirAll(absDir, os.ModeDir|os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	return absDir
}

func generateAttachName(ext string, fileType int) string {
	name := strconv.FormatInt(time.Now().UnixMilli(), 10) + util.RandomStr(5) + ext
	return name
}

//实现登录验证,需要判断登录的contoller继承该结构体
type AuthController struct {
	BaseController
}

func (c *AuthController) Prepare() {
	//验证用户是否登录
	//fmt.Println("Prepare方法:" + c.Ctx.Request.RequestURI)
	loginUser := c.GetSession("curUser")
	if loginUser == nil {
		logs.Debug("get loginuser fail from session")
		if orm.Debug {
			loginUser = models.GetUserById(1)
			c.SetSession("curUser", loginUser)
		} else {
			if c.IsAjax() {
				c.Abort("401")
			} else {
				c.Redirect("/login", 302)
			}
		}
	}
	c.Data["loginUser"] = loginUser
}

func (c *AuthController) getLoginUser() (*models.UserInfo, error) {
	cacheUser := c.GetSession("curUser")
	if cacheUser != nil {
		loginUser, ok := cacheUser.(*models.UserInfo)
		if ok {
			return loginUser, nil
		}
	}
	return nil, errors.New("获取登录用户失败")
}
