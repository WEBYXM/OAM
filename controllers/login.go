package controllers

import (
	"OAM/conf"
	"OAM/models"
	fn "OAM/util"
	"encoding/base64"
	"net/url"
	"regexp"
	"time"
)

type LoginController struct {
	BaseController
}

func (c *LoginController) Get() {
	c.TplName = "login.html"
}

func (c *LoginController) Post() {
	//参数验证
	uname := c.GetString("uname")
	upwd := c.GetString("upwd")
	if uname == "" || upwd == "" {
		c.JsonParamError("用户名或密码错误")
	}
	if isValid, err := regexp.MatchString(`^[a-z0-9][\w@.]{1,28}[a-z0-9]$`, uname); err != nil && !isValid {
		c.JsonParamError("用户名不正确")
	}
	if len(upwd) < 6 {
		c.JsonParamError("密码不正确")
	}

	var failedQty = getLoginFailedQty(uname)
	if failedQty > 5 {
		c.JsonFailed("账号已被禁止登录,请稍候再试")
	}
	//解密密码
	plainPwd, err := pwdDecode(upwd)
	if err != nil {
		cacheFailedQty(uname, failedQty == 0)
		c.JsonParamError("用户名或密码错误")
	}
	if isValid, err := regexp.MatchString(`^[\S]{6,30}$`, plainPwd); err != nil && !isValid {
		c.JsonParamError("密码不正确")
	}
	// 密码和用户状态验证
	user := models.GetUserByName(uname)
	if user != nil {
		if user.UserStatus == 0 {
			c.JsonParamError("用户已被禁用")
		}
		if user.CheckPasswd(plainPwd) {
			c.SessionRegenerateID()
			user.Passwd = ""
			c.SetSession("curUser", user)
			clearFailedQty(uname)
			c.JsonOk(nil)
			return
		}
	}
	cacheFailedQty(uname, failedQty == 0)
	c.JsonParamError("用户名或密码错误")
}

func (c *LoginController) Logout() {
	c.DestroySession()
	c.Redirect("/login", 302)
}

//返回客户端公钥
func (c *BaseController) Pubkey() {
	c.justPost()
	key, err := fn.PublicKeyToBase64(conf.GlobalCfg.RSA_DEFAULT_PUBLIC_KEY)
	if err != nil {
		c.JsonFailed("公钥加载失败")
		return
	}
	c.JsonOk(key)
}

func (c *BaseController) Prikey() {
	c.justPost()
	key, err := fn.PrivateKeyToBase64(conf.GlobalCfg.RSA_DEFAULT_PRIVATE_KEY)
	if err != nil {
		c.JsonFailed("客户端私钥加载失败")
		return
	}
	c.JsonOk(key)
}

func getCache() fn.MyCache {
	return fn.GetCache(fn.CACHE_LOGIN_FAILED)
}

//获取用户已登录失败次数
func getLoginFailedQty(userName string) int {
	return getCache().GetInt(userName)
}

//缓存登录错误次数
func cacheFailedQty(userName string, isFirst bool) {
	if isFirst {
		getCache().Put(userName, 1, 10*time.Minute)
	} else {
		getCache().Incr(userName)
	}
}

func clearFailedQty(userName string) {
	getCache().Delete(userName)
}

//解码密码
func pwdDecode(pwd string) (string, error) {
	tmp, err := base64.StdEncoding.DecodeString(pwd)
	if err == nil {
		tmpStr, err := url.QueryUnescape(string(tmp))
		if err == nil {
			tmpStr = fn.Reverse(tmpStr[0 : len(tmpStr)-5])
			tmp, err := base64.StdEncoding.DecodeString(tmpStr)
			if err == nil {
				tmpStr, _ = url.QueryUnescape(string(tmp))
				return url.QueryUnescape(tmpStr)
			}
		}
	}
	return "", err
}
