SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(150) NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `create_by` varchar(30) NOT NULL,
  `update_by` varchar(30) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `field_user` varchar(150) DEFAULT '' COMMENT '账号',
  `field_pwd` varchar(250) DEFAULT NULL COMMENT '账号密码',
  `field_url` varchar(255) DEFAULT NULL COMMENT '账号地址',
  `field_remark` varchar(1000) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '账号备注',
  `field_other` text CHARACTER SET utf8mb4 COMMENT '账号动态字段内容(JSON)',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `host_id` int(11) DEFAULT NULL,
  `rel_account_ids` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `idx_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_field
-- ----------------------------
DROP TABLE IF EXISTS `account_field`;
CREATE TABLE `account_field` (
  `field_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(100) NOT NULL,
  `field_key` varchar(50) NOT NULL,
  `type_id` int(11) NOT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_ciphertext` tinyint(1) NOT NULL DEFAULT '0',
  `max_len` int(10) unsigned NOT NULL,
  `value_type` smallint(6) NOT NULL DEFAULT '0' COMMENT '值类型:0=文本,1=布尔,2=数字,3=可选值',
  `value_rule` varchar(300) DEFAULT NULL COMMENT '取值规则json',
  `sort` smallint NOT NULL DEFAULT 0 COMMENT '排序',
  PRIMARY KEY (`field_id`),
  KEY `idx_fieldtype_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for account_type
-- ----------------------------
DROP TABLE IF EXISTS `account_type`;
CREATE TABLE `account_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for app_info
-- ----------------------------
DROP TABLE IF EXISTS `app_info`;
CREATE TABLE `app_info` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL DEFAULT '0',
  `app_name` varchar(50) NOT NULL,
  `app_url` varchar(255) DEFAULT NULL COMMENT '外部访问地址',
  `app_port` int(9) DEFAULT NULL COMMENT '占用端口',
  `app_dir` varchar(255) DEFAULT NULL COMMENT '安装目录',
  `sourcecode_repo` varchar(255) DEFAULT NULL COMMENT '源码地址',
  `desc` varchar(600) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `dev_lang` varchar(30) DEFAULT NULL COMMENT '开发语言',
  `app_type` varchar(30) NOT NULL DEFAULT '' COMMENT '业务应用, web服务器, 关系型数据库, 非关系数据库, 缓存, 消息队列, 文件存储服务, 搜索引擎, DevOps, 其它',
  PRIMARY KEY (`app_id`),
  KEY `idx_proj_id` (`proj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dict_item
-- ----------------------------
DROP TABLE IF EXISTS `dict_item`;
CREATE TABLE `dict_item` (
  `item_id` varchar(50) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_value` varchar(20000) DEFAULT NULL,
  `item_type` varchar(30) NOT NULL DEFAULT '',
  `update_time` datetime NOT NULL,
  `update_by` varchar(30) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for document
-- ----------------------------
DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `doc_type` varchar(5) NOT NULL COMMENT 'pdf,md...',
  `proj_id` int(11) NOT NULL DEFAULT '0',
  `author_id` int(30) NOT NULL,
  `create_by` varchar(50) NOT NULL,
  `update_by` varchar(30) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` text,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for function
-- ----------------------------
DROP TABLE IF EXISTS `function`;
CREATE TABLE `function` (
  `fun_id` int(11) NOT NULL AUTO_INCREMENT,
  `fun_name` varchar(30) NOT NULL COMMENT '功能名',
  `fun_code` varchar(50) NOT NULL DEFAULT '' COMMENT '功能权限唯一标识',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `fun_type` tinyint(4) NOT NULL COMMENT '1=菜单 2=功能 3=数据权限',
  `fun_order` smallint(6) NOT NULL DEFAULT '1',
  `fun_level` smallint(6) NOT NULL DEFAULT '1' COMMENT '树层级',
  `menu_class` varchar(200) DEFAULT NULL COMMENT '菜单css类',
  `fun_url` varchar(1000) DEFAULT NULL COMMENT '功能代表的url,多个以逗号分隔',
  `page_route` varchar(200) DEFAULT NULL COMMENT '页面路由',
  `create_time` datetime NOT NULL COMMENT '建立时间',
  `create_by` varchar(40) NOT NULL COMMENT '建立人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(40) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`fun_id`),
  UNIQUE KEY `uq_index_funcode` (`fun_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统功能表';

-- ----------------------------
-- Table structure for host_info
-- ----------------------------
DROP TABLE IF EXISTS `host_info`;
CREATE TABLE `host_info` (
  `host_id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(50) NOT NULL,
  `public_ip` varchar(32) DEFAULT NULL COMMENT '公网ip',
  `internal_ip` varchar(32) DEFAULT NULL COMMENT '内网ip',
  `ssh_port` int(11) DEFAULT NULL COMMENT '远程连接端口',
  `os_name` varchar(30) DEFAULT NULL COMMENT '操作系统',
  `create_time` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `host_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '服务器类型:0=通用服务器,1=数据库,2=Web应用,3=文件服务器',
  `desc` varchar(500) DEFAULT NULL COMMENT '说明备注',
  PRIMARY KEY (`host_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `proj_id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_name` varchar(50) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL COMMENT '创建人ID',
  `proj_desc` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`proj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rel_host_app
-- ----------------------------
DROP TABLE IF EXISTS `rel_host_app`;
CREATE TABLE `rel_host_app` (
  `host_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`host_id`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rel_proj_account
-- ----------------------------
DROP TABLE IF EXISTS `rel_proj_account`;
CREATE TABLE `rel_proj_account` (
  `proj_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`proj_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for rel_proj_user
-- ----------------------------
DROP TABLE IF EXISTS `rel_proj_user`;
CREATE TABLE `rel_proj_user` (
  `proj_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`proj_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `rel_proj_host`;
CREATE TABLE `rel_proj_host` (
  `proj_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  PRIMARY KEY (`proj_id`,`host_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for rel_role_function
-- ----------------------------
DROP TABLE IF EXISTS `rel_role_function`;
CREATE TABLE `rel_role_function` (
  `role_code` varchar(30) NOT NULL,
  `fun_id` int(11) NOT NULL,
  PRIMARY KEY (`role_code`,`fun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色功能关联表';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_code` varchar(30) NOT NULL COMMENT '角色CODE',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=禁用1=启用',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) NOT NULL,
  `passwd` varchar(250) NOT NULL,
  `real_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `role_code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 默认密码2022@00
INSERT INTO `user_info` (`user_id`, `user_name`, `passwd`, `real_name`, `user_status`, `create_time`, `role_code`) VALUES ('1', 'root', 'a3595a8039559093a5108ec841e7fe4794f1301337e79109e88ed93ea3929a2b', '超级管理员', '1', '2022-02-11 14:46:06', '');

INSERT INTO `sys_role` (`role_code`, `role_name`, `role_status`) VALUES ('root', '超管', '1');
INSERT INTO `sys_role` (`role_code`, `role_name`, `role_status`) VALUES ('ops', '运维', '1');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (1, '微信公众号', '2023-09-06 16:06:09', '2022-04-13 10:44:43');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (3, '微信商户账号', '2023-09-06 16:08:33', '2022-06-24 10:08:52');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (4, '支付宝商户账号', '2022-06-24 10:43:02', '2022-06-24 10:43:02');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (5, '支付宝应用账号', '2023-09-06 16:28:14', '2022-06-24 10:47:25');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (6, '微信小程序', '2023-09-06 16:28:46', '2022-06-24 10:53:41');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (7, '数据库账号', '2023-09-06 11:54:11', '2022-06-24 10:53:41');
INSERT INTO `account_type` (`type_id`, `type_name`, `update_time`, `create_time`) VALUES (8, '操作系统账号', '2022-06-24 10:53:41', '2022-06-24 10:53:41');

INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (12, '商户号', 'wxpay_mch_id', 2, 1, 0, 32, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (13, '应用appid', 'wxpay_app_id', 2, 1, 0, 32, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (14, '子应用appid', 'wxpay_sub_appid', 2, 0, 0, 32, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (15, '密钥app_secret', 'wxpay_app_secret', 2, 0, 0, 250, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (16, '密钥apikey', 'wxpay_api_key', 2, 0, 0, 250, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (28, '商户号PID', 'ali_pid', 4, 0, 0, 20, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (29, '操作密码', 'ali_optpasswd', 4, 0, 1, 30, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (30, '支付密码', 'ali_paypasswd', 4, 0, 1, 250, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (31, '绑定人', 'ali_binduser', 4, 0, 0, 50, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (32, '公司全称', 'ali_compay', 4, 0, 0, 250, 0, NULL, 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (66, 'appid', 'wx_app_id', 1, 1, 0, 50, 0, '', 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (67, '密钥appSecret', 'wx_app_secret', 1, 0, 0, 500, 0, '', 1);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (68, '账号绑定人', 'wx_bind_user', 1, 0, 0, 250, 0, '', 3);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (69, '主体信息', 'wx_company', 1, 0, 0, 50, 0, '', 4);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (70, 'API秘钥', 'mch_apikey', 3, 0, 0, 50, 0, '', 3);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (71, '操作密码', 'mch_optpasswd', 3, 0, 1, 250, 0, '', 5);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (72, 'API证书', 'mch_apicert', 3, 0, 0, 250, 4, '', 2);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (73, '绑定人', 'mch_binduser', 3, 0, 0, 250, 0, '', 6);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (74, '公司全称', 'mch_compay', 3, 0, 0, 250, 0, '', 1);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (75, '商户类型', 'mch_usertype', 3, 0, 0, 0, 3, '[{\"text\":\"服务商\",\"id\":1},{\"text\":\"普通商户\",\"id\":0},{\"text\":\"子商户\",\"id\":2}]', 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (88, '应用名', 'alipay_appname', 5, 0, 0, 100, 0, '', 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (89, '应用appid', 'alipay_appid', 5, 1, 0, 250, 0, '', 1);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (90, '支付宝公钥', 'alipay_publickey', 5, 0, 0, 4000, 0, '', 2);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (91, '私钥', 'alipay_private_key', 5, 0, 0, 4000, 0, '', 3);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (92, '接口密钥(AES)', 'alipay_aeskey', 5, 0, 0, 30, 0, '', 5);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (93, 'appid', 'wx_miniappid', 6, 1, 0, 32, 0, '', 0);
INSERT INTO `account_field` (`field_id`, `field_name`, `field_key`, `type_id`, `is_required`, `is_ciphertext`, `max_len`, `value_type`, `value_rule`, `sort`) VALUES (94, '密钥appsecret', 'wx_miniapp_secret', 6, 0, 0, 32, 0, '', 0);

INSERT INTO `dict_item` (`item_id`, `item_name`, `item_value`, `item_type`, `update_time`, `update_by`) VALUES ('os_names', '操作系统名称', 'Centos6,Centos7,Ubuntu,RedHat,Linux,Windows server 2008,Windows server 2012,Windows server 2016,Windows server 2019,Windows server', '', '2022-05-26 16:41:04', 'sys');
INSERT INTO `dict_item` (`item_id`, `item_name`, `item_value`, `item_type`, `update_time`, `update_by`) VALUES ('service_softwares', '服务软件列表', '[\r\n{\"appName\":\"Mysql\",\"appPort\":3306,\"appType\":\"关系型数据库\",\"devLang\":\"C++\"},\r\n{\"appName\":\"SqlServer\",\"appPort\":1433,\"appType\":\"关系型数据库\"},\r\n{\"appName\":\"Oracle\",\"appPort\":1521,\"appType\":\"关系型数据库\"},\r\n{\"appName\":\"Postgresql\",\"appPort\":5432,\"appType\":\"关系型数据库\"},\r\n{\"appName\":\"MongoDB\",\"appPort\":27017,\"appType\":\"非关系型数据库\"},\r\n{\"appName\":\"HBase\",\"appPort\":16020,\"appType\":\"非关系型数据库\"},\r\n{\"appName\":\"Cassandra\",\"appPort\":7199,\"appType\":\"非关系型数据库\"},\r\n{\"appName\":\"ClickHouse\",\"appPort\":9000,\"appType\":\"非关系型数据库\"},\r\n{\"appName\":\"Nginx\",\"appPort\":80,\"appType\":\"web服务器\"},\r\n{\"appName\":\"Tomcat\",\"appPort\":8080,\"appType\":\"web服务器\"},\r\n{\"appName\":\"IIS\",\"appPort\":80,\"appType\":\"web服务器\"},\r\n{\"appName\":\"Redis\",\"appPort\":6379,\"appType\":\"缓存\"},\r\n{\"appName\":\"Memcached\",\"appPort\":11211,\"appType\":\"缓存\"},\r\n{\"appName\":\"FastDFS\",\"appPort\":22122,\"appType\":\"文件存储服务\"},\r\n{\"appName\":\"MinIO\",\"appPort\":9000,\"appType\":\"文件存储服务\"},\r\n{\"appName\":\"HDFS\",\"appPort\":50070 ,\"appType\":\"文件存储服务\"},\r\n{\"appName\":\"RabbitMQ\",\"appPort\":5672 ,\"appType\":\"消息队列\"},\r\n{\"appName\":\"Kafka\",\"appPort\":9092 ,\"appType\":\"消息队列\"},\r\n{\"appName\":\"RocketMQ\",\"appPort\":9876  ,\"appType\":\"消息队列\"},\r\n{\"appName\":\"ActiveMQ\",\"appPort\":61616 ,\"appType\":\"消息队列\"},\r\n{\"appName\":\"Apache Pulsar\",\"appPort\":6650 ,\"appType\":\"消息队列\"},\r\n{\"appName\":\"ElasticSearch\",\"appPort\":9200 ,\"appType\":\"搜索引擎\"},\r\n{\"appName\":\"Solr\",\"appPort\":8983 ,\"appType\":\"搜索引擎\"},\r\n{\"appName\":\"Jenkins\",\"appPort\":9090 ,\"appType\":\"DevOps\"},\r\n{\"appName\":\"Docker\",\"appType\":\"DevOps\"},\r\n{\"appName\":\"K8S\",\"appType\":\"DevOps\"},\r\n{\"appName\":\"Nacos\",\"appPort\":8848 ,\"appType\":\"其它\"},\r\n{\"appName\":\"Zookeeper\",\"appPort\":2181 ,\"appType\":\"其它\"}\r\n]', '', '2022-05-27 15:57:52', 'sys');
