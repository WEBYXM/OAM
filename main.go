package main

import (
	"OAM/conf"
	"OAM/controllers"
	"OAM/models"
	_ "OAM/routers"
	fn "OAM/util"

	"errors"
	"os"
	"path/filepath"

	"github.com/beego/beego/v2/core/logs"
	beego "github.com/beego/beego/v2/server/web"
	jsoniter "github.com/json-iterator/go"
)

func main() {
	initLog()

	go initGlobalCfg()

	storePath := beego.AppConfig.DefaultString("store_path", "")
	if storePath == "" {
		storePath, err := fn.UserHomeDir()
		if err != nil {
			panic(err)
		}
		conf.GlobalCfg.DATA_STORE_PATH = filepath.Join(storePath, "/.oam")
	} else {
		conf.GlobalCfg.DATA_STORE_PATH = storePath
	}
	if !fn.FileIsExists(conf.GlobalCfg.DATA_STORE_PATH) {
		os.MkdirAll(conf.GlobalCfg.DATA_STORE_PATH, os.ModePerm)
	}
	beego.SetStaticPath(conf.STATIC_EXT_BASE_URL, conf.GlobalCfg.DATA_STORE_PATH)

	beego.ErrorController(&controllers.ErrorController{})
	beego.AddFuncMap("json", func(in interface{}) (string, error) {
		return jsoniter.MarshalToString(in)
	})

	beego.Run()
}

//初始配置日志组件
func initLog() {
	isDebug, _ := beego.AppConfig.Bool("debug")
	if !isDebug {
		err := logs.SetLogger(logs.AdapterFile, `{"filename":"logs/opsdoc.log","level":6,"maxdays":10,"daily":true}`)
		if err != nil {
			panic(err)
		}
		logs.EnableFuncCallDepth(true)
		logs.Async(1e3)
	}
}

// 初始一些通用配置
func initGlobalCfg() {
	var err error
	//当前程序工作目录
	workDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	//conf.GlobalCfg.WORK_PATH = workDir

	// 缓存引擎类型
	conf.GlobalCfg.CACHE_TYPE = beego.AppConfig.DefaultString("cache_type", "memory")

	// 加载RSA密钥文件
	logs.Info("加载密钥文件")
	priKeyPath := filepath.Join(workDir, "conf", "pri.pem")
	conf.GlobalCfg.RSA_DEFAULT_PRIVATE_KEY, err = fn.LoadPrivateKeyFromFile(priKeyPath)
	if err != nil {
		panic(errors.New("加载私钥文件失败"))
	}
	pubKeyPath := filepath.Join(workDir, "conf", "pub.pem")
	conf.GlobalCfg.RSA_DEFAULT_PUBLIC_KEY, err = fn.LoadPublicKeyFromFile(pubKeyPath)
	if err != nil {
		panic(errors.New("加载公钥文件失败"))
	}

	//初始AES密钥
	conf.GlobalCfg.SYMMETRIC_KEY = models.InitSymmetrickey()

}
