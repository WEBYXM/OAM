package models

import (
	"context"
	"strconv"
	"time"

	fn "OAM/util"

	"github.com/beego/beego/v2/client/orm"
	"github.com/beego/beego/v2/core/validation"
)

/* 应用信息 */
type AppInfo struct {
	AppId          int    `orm:"PK;auto"`
	AppName        string `valid:"Required;MaxSize(50)"`
	AppUrl         string `valid:"MaxSize(255)"`
	AppDir         string `valid:"MaxSize(255)"`
	AppPort        int
	AppType        string
	ProjId         int
	SourcecodeRepo string
	Desc           string    `valid:"MaxSize(600)"`
	CreateTime     time.Time `orm:"auto_now_add;type(datetime)" json:",omitempty"`
	DevLang        string
	HostIps        string `orm:"-"`
}

type AppInfoVO struct {
	AppInfo
	HostIds fn.IntList
}

// 常用编程语言
var DevLangs = [14]string{"Java", "C", "C++", "C#", "JavaScript", "Go", "PHP", "Python", "VB.net", "Swift", "Kotlin", "RUST", "Scala", "Erlang"}
var AppTypes = []string{"业务应用", "web服务器", "关系型数据库", "NoSQL", "缓存", "消息队列", "文件存储服务", "搜索引擎", "DevOps", "其它"}

const table_app_name = "app_info"

func (t *AppInfo) TableName() string {
	return table_app_name
}

func (acct *AppInfo) Valid() string {
	valid := validation.Validation{}
	_, err := valid.Valid(acct)
	if err != nil {
		return err.Error()
	}
	return toErrMsg(valid)
}

func GetAppInfoById(id int) *AppInfo {
	app := AppInfo{AppId: id}
	query := orm.NewOrm()
	err := query.Read(&app)
	if checkQueryErr(err) {
		return nil
	}
	return &app
}

func GetAppInfoVOById(id int) *AppInfoVO {
	app := AppInfo{AppId: id}
	query := orm.NewOrm()
	err := query.Read(&app)
	if checkQueryErr(err) {
		return nil
	}
	appVO := AppInfoVO{AppInfo: app}
	var ids orm.ParamsList
	num, err := query.Raw("select host_id from rel_host_app where app_id=?", id).ValuesFlat(&ids)
	if err == nil && num > 0 {
		appVO.HostIds = ToIntSlice(ids)
	}
	return &appVO
}

func FindAppInfoByHostId(hostId int) []*AppInfo {
	var apps []*AppInfo
	_, err := orm.NewOrm().Raw("select a.* from app_info a join rel_host_app rel on a.app_id=rel.app_id where rel.host_id=?", hostId).QueryRows(&apps)
	if checkQueryErr(err) {
		return nil
	}
	return apps
}

func FindAppInfoByProjId(projId int) []*AppInfo {
	var apps []*AppInfo
	query := orm.NewOrm()
	selector := query.QueryTable(table_app_name)
	_, err := selector.Filter("proj_id", projId).All(&apps)
	if checkQueryErr(err) {
		return nil
	}
	return apps
}

func FindAppInfoForPage(row int, curpage int, condi map[string]interface{}) Page[AppInfo] {
	pageData := Page[AppInfo]{RowPerPage: row, PageNum: curpage}
	query := orm.NewOrm()
	selector := query.QueryTable(table_app_name)
	if condi != nil {
		where := orm.NewCondition()
		kw, exist := condi["keyword"]
		if exist {
			where = where.And("app_name__icontains", kw.(string))
		}
		projId, exist := condi["projId"]
		if exist {
			where = where.And("proj_id", projId.(int))
		}
		appType, exist := condi["appType"]
		if exist {
			where = where.And("app_type", appType.(string))
		}

		selector = selector.SetCond(where)
	}
	pageData.QueryPage(selector.OrderBy("-app_id"))
	//查出关联的主机名
	if len(pageData.Rows) > 0 {
		var ids []int
		for _, app := range pageData.Rows {
			ids = append(ids, app.AppId)
		}
		var maps []orm.Params
		sql := `select a.app_id,GROUP_CONCAT(DISTINCT h.internal_ip) host_ip from app_info a join rel_host_app rh on a.app_id=rh.app_id
		join host_info h on rh.host_id=h.host_id where a.app_id in (` + fn.JoinInteger(",", ids...) + `) GROUP BY a.app_id`
		num, err := orm.NewOrm().Raw(sql).Values(&maps)
		if err == nil && num > 0 {
			for _, app := range pageData.Rows {
				for _, rel := range maps {
					if strconv.Itoa(app.AppId) == rel["app_id"] {
						app.HostIps = rel["host_ip"].(string)
					}
				}
			}
		}
	}
	return pageData
}

func SaveAppInfo(appVO *AppInfoVO) error {
	return orm.NewOrm().DoTx(func(ctx context.Context, txOrm orm.TxOrmer) error {
		_, err := txOrm.Insert(&appVO.AppInfo)
		if err != nil {
			return err
		}
		if len(appVO.HostIds) > 0 {
			p, err1 := txOrm.Raw("insert into rel_host_app (host_id,app_id) values(?,?)").Prepare()
			if err1 != nil {
				return err1
			}
			defer p.Close()
			for _, hostId := range appVO.HostIds {
				_, err1 = p.Exec(hostId, appVO.AppId)
				if err1 != nil {
					return err1
				}
			}
		}
		return nil
	})
}

func UpdateAppInfo(appVO *AppInfoVO) error {
	return orm.NewOrm().DoTx(func(ctx context.Context, txOrm orm.TxOrmer) error {
		_, err := txOrm.Update(&appVO.AppInfo)
		if err != nil {
			return err
		}
		if len(appVO.HostIds) > 0 {
			txOrm.Raw("delete from rel_host_app where app_id=?", appVO.AppId).Exec()
			p, err1 := txOrm.Raw("insert into rel_host_app (host_id,app_id) values(?,?)").Prepare()
			if err1 != nil {
				return err1
			}
			defer p.Close()
			for _, hostId := range appVO.HostIds {
				_, err1 = p.Exec(hostId, appVO.AppId)
				if err1 != nil {
					return err1
				}
			}
		}
		return nil
	})
}

func DeleteAppInfo(id int) bool {
	app := AppInfo{AppId: id}
	err := orm.NewOrm().DoTx(func(ctx context.Context, txOrm orm.TxOrmer) error {
		i, err := txOrm.Delete(&app)
		if err == nil && i > 0 {
			txOrm.Raw("delete from rel_host_app where app_id=?", id).Exec()
		}
		return err
	})
	return err == nil
}
